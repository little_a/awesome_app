from flask import Flask
import socket
app = Flask(__name__)

@app.route('/')
def hello_world():
    return "I'm web-app hosted by " + socket.gethostname()
