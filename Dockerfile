FROM python:alpine
EXPOSE 5000
ADD app.py /app/app.py
ADD requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt
ENV FLASK_APP /app/app.py
ENTRYPOINT ["flask", "run", "--host=0.0.0.0"]
