job "awesome-app-job" {
  type = "service"
  datacenters = ["dc1"]
  update {
    stagger = "30s"
  }
  group "awesome-app-group" {
    count = 2
    task "awesome_app" {
      driver = "docker"
      config {
        image = "registry.gitlab.com/little_a/awesome_app:latest"
        port_map {
          http = 5000
        }
      }
      service {
        name = "awesome-app-service"
        port = "http"
        check {
          type = "http"
          protocol = "http"
          path = "/"
          interval = "10s"
          timeout = "2s"
        }
      }
      resources {
        network {
          port "http" {}
        }
      }
    }
  }
}
